package yamin.com.company.Processor;

/*
    Created in IntelliJ IDEA.
    Project: AfterPayTest
    Package: yamin.com.company
    User: YaminBin
    Date: 27/04/2019 : 7:06 PM
*/

import yamin.com.company.Entity.Transaction;
import yamin.com.company.Utils.Utils;

import java.io.*;
import java.io.BufferedReader;
import java.util.*;
import java.util.stream.Collectors;

public class TransactionProcessor {

    private String fileURL;
    private int windowSize;
    private float windowAmountLimit;
    private List<Transaction> transactions = new ArrayList();
    private Utils utils = Utils.getInstance();

    public TransactionProcessor() {
        this.fileURL = utils.FILE_LOCATION + utils.FILE_NAME;
        this.windowSize = utils.WINDOW_SIZE;
        this.windowAmountLimit = utils.WINDOW_AMOUNT_LIMIT;
    }

    public void processData() {

        boolean fraudFound = false;
        // Get distinct objects by credit card number
        List<String> distinctCreditCards = this.transactions.stream()
                .filter(utils.distinctByKey(t -> t.getCreditCardNumber()))
                .map(Transaction::getCreditCardNumber)
                .collect(Collectors.toList());

        // Get the group of date sorted transactions for each of the distinct credit cards
        List<Transaction> groupedTransactions = new ArrayList<>();
        for (String dcc : distinctCreditCards) {
            groupedTransactions = this.transactions.stream()
                    .filter(t -> t.getCreditCardNumber().equals(dcc))
                    .sorted(Comparator.comparing(Transaction::getTransactionDate))
                    .collect(Collectors.toList());

            for (Transaction t : groupedTransactions) {
                Date windowStart = t.getTransactionDate();
                Date windowEnd = utils.calculateWindow(windowStart);
                // Get all the transactions that is in between the current window
                List<Transaction> windowedTransactions = groupedTransactions.stream()
                        .filter(gt -> (gt.getTransactionDate().after(windowStart) && gt.getTransactionDate().before(windowEnd)
                                || (gt.getTransactionDate().equals(windowStart) || gt.getTransactionDate().equals(windowEnd))))
                        .collect(Collectors.toList());
                // Sum of the transaction amounts in that window
                float windowSum = (float) windowedTransactions.stream().mapToDouble(Transaction::getAmount).sum();
                if (windowSum > utils.WINDOW_AMOUNT_LIMIT) {
                    System.out.println("Credit Card - " + dcc + " is fraudulent");
                    fraudFound = true;
                    break;
                }
            }
        }
        if (!fraudFound) {
            System.out.println("No Fraudulent Credit Cards Found!");
        }
    }

    public void readFile() throws Exception {
        File file = new File(this.fileURL);

        BufferedReader br = new BufferedReader(new FileReader(file));

        String inputLine;
        while ((inputLine = br.readLine()) != null) {
            String[] inputs = inputLine.split(utils.INPUT_SPLIT);

            String creditcardNumber = inputs[0];
            Date date = utils.convertToDate(inputs[1]);
            String amount = inputs[2];

            Transaction transaction = new Transaction(creditcardNumber, date, Float.parseFloat(amount));
            this.transactions.add(transaction);
        }
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = utils.FILE_LOCATION + fileURL;
    }

    public int getWindowSize() {
        return windowSize;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public float getWindowAmountLimit() {
        return windowAmountLimit;
    }

    public void setWindowAmountLimit(float windowAmountLimit) {
        this.windowAmountLimit = windowAmountLimit;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}