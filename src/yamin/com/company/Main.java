package yamin.com.company;

/*
    Created in IntelliJ IDEA.
    Project: AfterPayTest
    Package: yamin.com.company
    User: YaminBin
    Date: 27/04/2019 : 7:06 PM
*/

import yamin.com.company.Processor.TransactionProcessor;

public class Main {

    public static void main(String[] args) {

        TransactionProcessor transactionProcessor = new TransactionProcessor();

        try {
            // fetch text file name from command line if present
            if (args.length > 0) {
                transactionProcessor.setFileURL(args[0]);
                transactionProcessor.setWindowSize(Integer.parseInt(args[1]));
                transactionProcessor.setWindowAmountLimit(Float.parseFloat(args[2]));
            }
            transactionProcessor.readFile();
            transactionProcessor.processData();
        }
        catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }
}
