package yamin.com.company.Utils;

/*
    Created in IntelliJ IDEA.
    Project: AfterPayTest
    Package: yamin.com.company.Utils
    User: YaminBin
    Date: 27/04/2019 : 8:23 PM
*/

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

public class Utils {
    private static Utils ourInstance = new Utils();

    public static Utils getInstance() {
        return ourInstance;
    }

    private Utils() {
    }

    public static final int WINDOW_SIZE = 24;
    public static final float WINDOW_AMOUNT_LIMIT = 50;
    public static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String INPUT_SPLIT = ",";
    public static final String FILE_LOCATION = "./Resources/";
    public static final String FILE_NAME = "Transactions.txt";

    public static Date convertToDate(String input) throws ParseException {
        String[] inputDate = input.split("T");
        SimpleDateFormat formatter = new SimpleDateFormat(TIME_FORMAT);
        Date date = formatter.parse(inputDate[0] + " " + inputDate[1]);
        return date;
    }

    public static Date calculateWindow(Date windowStart) {
        Calendar calender = Calendar.getInstance();
        calender.setTime(windowStart);
        calender.add(Calendar.HOUR_OF_DAY, WINDOW_SIZE);
        return calender.getTime();
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static <E> void displayList(List<E> list) {
        for (E o : list) {
            System.out.println(o);
        }
    }
}
