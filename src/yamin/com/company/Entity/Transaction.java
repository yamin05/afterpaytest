package yamin.com.company.Entity;

/*
    Created in IntelliJ IDEA.
    Project: AfterPayTest
    Package: yamin.com.company.Entity
    User: YaminBin
    Date: 27/04/2019 : 8:05 PM
*/

import java.util.Date;

public class Transaction {

    private String creditCardNumber;
    private Date transactionDate;
    private float amount;

    public Transaction(String creditCardNumber, Date transactionDate, float amount) {
        this.creditCardNumber = creditCardNumber;
        this.transactionDate = transactionDate;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "creditCardNumber='" + creditCardNumber + '\'' +
                ", transactionDate=" + transactionDate +
                ", amount=" + amount +
                '}';
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
