package yamin.com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import yamin.com.company.Entity.Transaction;
import yamin.com.company.Processor.TransactionProcessor;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/*
    Created in IntelliJ IDEA.
    Project: AfterPayTest
    Package: yamin.com.company
    User: YaminBin
    Date: 30/04/2019 : 11:39 PM
*/

class TransactionProcessorTest {

    private static TransactionProcessor tp;
    private static List<Transaction> list;

    @BeforeAll
    static void setup() {
        tp = new TransactionProcessor();
        tp.setFileURL("TransactionsTest.txt");
    }

    @BeforeEach
    void BeforeEach() {
        list = new ArrayList();
    }

    @Test
    void processData() throws ParseException {
        Transaction transaction1 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-28 14:15:54"), 10);
        Transaction transaction2 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 12:25:54"), 20);
        Transaction transaction3 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-30 14:55:54"), 30);
        Transaction transaction4 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-28 10:45:54"), 40);
        Transaction transaction5 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-28 16:35:54"), 10);
        Transaction transaction6 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 17:24:54"), 30);
        Transaction transaction7 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 19:20:54"), 50);
        Transaction transaction8 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 11:19:54"), 60);
        list.add(transaction1);
        list.add(transaction2);
        list.add(transaction3);
        list.add(transaction4);
        list.add(transaction5);
        list.add(transaction6);
        list.add(transaction7);
        list.add(transaction8);
        tp.setTransactions(list);

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));

        tp.processData();

        String s = "Credit Card - 10d7ce2f43e35fa57d1bbf8b1e3 is fraudulent\r\n";

        Assertions.assertEquals(s, outContent.toString());

        System.setOut(originalOut);
    }

    @Test
    void readFile() throws Exception {
        Transaction transaction1 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-28 14:15:54"), 10);
        Transaction transaction2 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 12:25:54"), 20);
        Transaction transaction3 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-30 14:55:54"), 30);
        Transaction transaction4 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-28 10:45:54"), 40);
        Transaction transaction5 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-28 16:35:54"), 10);
        Transaction transaction6 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 17:24:54"), 30);
        Transaction transaction7 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 19:20:54"), 50);
        Transaction transaction8 = new Transaction("10d7ce2f43e35fa57d1bbf8b1e3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 11:19:54"), 60);
        list.add(transaction1);
        list.add(transaction2);
        list.add(transaction3);
        list.add(transaction4);
        list.add(transaction5);
        list.add(transaction6);
        list.add(transaction7);
        list.add(transaction8);
        tp.readFile();
        Assertions.assertEquals(tp.getTransactions().toString(), list.toString());
    }
}