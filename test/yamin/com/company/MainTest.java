package yamin.com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/*
    Created in IntelliJ IDEA.
    Project: AfterPayTest
    Package: yamin.com.company
    User: YaminBin
    Date: 28/04/2019 : 9:48 PM
*/

class MainTest {
    @Test
    void main() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));

        String[] args = {"TransactionsTest.txt", "24", "50"};

        Main.main(args);

        String s = "Credit Card - 10d7ce2f43e35fa57d1bbf8b1e3 is fraudulent\r\n";
        Assertions.assertEquals(s, outContent.toString());

        System.setOut(originalOut);
    }
}