package yamin.com.company.Utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import yamin.com.company.Entity.Transaction;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
    Created in IntelliJ IDEA.
    Project: AfterPayTest
    Package: yamin.com.company.Utils
    User: YaminBin
    Date: 28/04/2019 : 8:21 PM
*/

class UtilsTest {

    private static Utils utils;

    @BeforeAll
    static void setup(){
        utils = Utils.getInstance();
    }

    @Test
    void getInstance() {
        Utils utils2 = Utils.getInstance();
        Assertions.assertEquals(utils, utils2);
    }

    @Test
    void convertToDate() throws ParseException {
        String sDate = "2014-04-29T17:24:54";
        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 17:24:54");
        Assertions.assertEquals(utils.convertToDate(sDate), date);
    }

    @Test
    void convertToDateException() throws ParseException {
        String sDate = "2014-04-29TEST17:24:54";
        Assertions.assertThrows(ParseException.class, () -> {utils.convertToDate(sDate);});
    }

    @Test
    void calculateWindow() throws ParseException {
        Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-11-27 17:24:54");
        Date date2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-11-28 17:24:54");
        Assertions.assertEquals(utils.calculateWindow(date1), date2);
    }

    @Test
    void displayList() throws ParseException {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));

        Transaction transaction1 = new Transaction("1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-27 17:24:54"), 10);
        Transaction transaction2 = new Transaction("2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-28 17:24:54"), 20);
        Transaction transaction3 = new Transaction("3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2014-04-29 17:24:54"), 30);

        String s = "Transaction{creditCardNumber='1', transactionDate=Sun Apr 27 17:24:54 AEST 2014, amount=10.0}\r\n";
        s+="Transaction{creditCardNumber='2', transactionDate=Mon Apr 28 17:24:54 AEST 2014, amount=20.0}\r\n";
        s+="Transaction{creditCardNumber='3', transactionDate=Tue Apr 29 17:24:54 AEST 2014, amount=30.0}\r\n";

        List<Transaction> list = new ArrayList();
        list.add(transaction1);
        list.add(transaction2);
        list.add(transaction3);

        utils.displayList(list);

        Assertions.assertEquals(s, outContent.toString());

        System.setOut(originalOut);
    }
}